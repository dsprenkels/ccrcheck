# ccrcheck

_ccrcheck_ is a tool for checking if coformer structures from the CSD occur in
cocrystal structures. For a chain of usage, one is best referred to the
`interactive_ccrcheck` script, as it describes the normal usage of ccrcheck,
from begin to end.

## Dependencies
> For FNWI users: these programs are preinstalled on the _lilo_-servers.

- _Python3_ (`sudo apt-get install python3`)
- _Open Babel_ (`sudo apt-get install openbabel`)

## Usage
At the moment of writing, ccrcheck allows three commands: `generate-can`,
`smi2can` and `verify-can`.

`generate-can` is used to generate CAN strings from SD files (make sure you use
the `exclude disordered atoms` in ConQuest). It is practically a wrapper around
Open Babel. It is recommended to use this wrapper, instead of calling Babel
directly, because in rare cases Babel tries to allocate all the memory of the
machine it runs on, often resulting in an unresponsive system. `generate-can`
limits the memory resources of Babel and maintains a timeout while converting
each SD structure. For all the options, refer to `python3 ccrcheck.py generate-can --help`.

`generate-can-ext` is an extension to `generate-can` which allows you to
generate CAN strings from SD files and immediately verify them. You don't need
to provide a list of triplets with this command. The `generate-can-ext` command
outputs a list of the produced CAN strings or an error code if the string could
not be converted.

`smi2can` is used to convert SMILES strings to can strings.

`verify-can` is used to verify the generated CAN strings. It will output to a
file in the format specified below.

## Output format
```plain
ABEKUN	AZSTBB	RESORA03	["result/match", null, "('AZSTBB', 'RESORA03') all occured in ABEKUN"]
ABEKUN	AZSTBB	RESORA03	["result/match", null, "('AZSTBB', 'RESORA03') all occured in ABEKUN"]
ABEKUN	EVUSIX	RESORA03	["result/mismatch", ["EVUSIX"], "['EVUSIX'] did not occur in ABEKUN"]
ABEKUN	EVUSIX	RESORA03	["result/mismatch", ["EVUSIX"], "['EVUSIX'] did not occur in ABEKUN"]
```

The first part of this line is shows the CSD refcodes in a canonicalised
(sorted and duplicate entries excluded) manner. After this, seperated by a tab
(`"\t"`) the result is printed. It is in JSON format and is in the following
format:

```javascript
[status, params, message]
```

Status can be one of the following:
- `'result/match'`:
    A match was found.
- `'result/mismatch'`:
    A mismatch occured. `params` will contain a list of coformer refcodes that
    did not match to the cocrystal structure.
- `'skip/panick'`:
    This record was skipped, because Open Babel was killed (by a SIGKILL)
    during during the conversion of the strucres in `params`. This probably
    means the input SD file was unreadable.
- `'skip/nocoords'`:
    This record was skipped, because there where no coordinates provided for
    some structures. The refcodes of these structures are supplied in `params`.
- `'skip/notonestring'`:
    This record was skipped, because of the coformer structures provided in
    `params` it it was not possible to extract only one CAN string. This is
    mostly the case when one of the coformer structures is really a cocrystal
    structure.
- `'skip/invcan'`:
    This record was skipped, because the produced CAN string did not match to
    the reference SMILES string from the CSD. In these cases is assumed that
    Babel produced an invalid CAN string. `params` will contain the refcodes
    of the invalid CAN strings.

As described above, the value of `params` depends on the value of `status`.

`message` will contain a more human-readable explanation with the status
code and should not be used for parsing.
