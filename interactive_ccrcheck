#!/bin/bash

hash babel 2>/dev/null || {
  echo >&2 "This program requires babel to be installed, exiting."
  exit 1
}

hash obabel 2>/dev/null || {
  echo >&2 "This program requires obabel to be installed, exiting."
  exit 1
}

CCRCHECK=$(dirname $0)/ccrcheck.py
LOGFILE=$(dirname $0)/interactive_ccrcheck.log

read -e -p "[1/6] Provide LST input file: " lstfile
read -e -p "[2/6] Provide chemical table (.sd) file: " sdfile
read -e -p "[3/6] Provide reference smiles (.smi) file: " csd_smifile
read -e -p "[4/6] Where should the produced CAN smiles be written to? " -i "${sdfile%.*}.can" canfile
read -e -p "[5/6] Where should the reference CAN smiles be cached? " -i "${csd_smifile%.*}.can" csd_canfile
read -e -p "[6/6] Finally, where shall I put the output file? " -i "${lstfile%.*}.out" outfile

set -o verbose

# convert the reference SMILES
"$CCRCHECK" smi2can "$csd_smifile" "$csd_canfile" 2>>"$LOGFILE"

# create the can files using babel
"$CCRCHECK" generate-can "$sdfile" "$canfile" 2>>"$LOGFILE"

# verify the CAN strings
"$CCRCHECK" verify-can --can "$canfile" --lst "$lstfile" --reference-smi "$csd_canfile" --out "$outfile"
