#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import subprocess

ATOMS = frozenset(['n', 'H', 'He', 'Li', 'Be', 'B', 'C', 'N', 'O', 'F', 'Ne',
'Na', 'Mg', 'Al', 'Si', 'P', 'S', 'Cl', 'Ar', 'K', 'Ca', 'Sc', 'Ti', 'V',
'Cr', 'Mn', 'Fe', 'Co', 'Ni', 'Cu', 'Zn', 'Ga', 'Ge', 'As', 'Se', 'Br', 'Kr',
'Rb', 'Sr', 'Y', 'Zr', 'Nb', 'Mo', 'Tc', 'Ru', 'Rh', 'Pd', 'Ag', 'Cd', 'In',
'Sn', 'Sb', 'Te', 'I', 'Xe', 'Cs', 'Ba', 'La', 'Ce', 'Pr', 'Nd', 'Pm', 'Sm',
'Eu', 'Gd', 'Tb', 'Dy', 'Ho', 'Er', 'Tm', 'Yb', 'Lu', 'Hf', 'Ta', 'W', 'Re',
'Os', 'Ir', 'Pt', 'Au', 'Hg', 'Tl', 'Pb', 'Bi', 'Po', 'At', 'Rn', 'Fr', 'Ra',
'Ac', 'Th', 'Pa', 'U', 'Np', 'Pu', 'Am', 'Cm', 'Bk', 'Cf', 'Es', 'Fm', 'Md',
'No', 'Lr', 'Rf', 'Db', 'Sg', 'Bh', 'Hs', 'Mt', 'Ds', 'Rg', 'Cn', 'Uuq',
'Uuh'])

MEMOIZATION_CACHE = {}

def log(x):
    print >> sys.stderr, x

def memoize(func):
    def memoized_func(*args):
        key = (func,) + args
        if key not in MEMOIZATION_CACHE:
            MEMOIZATION_CACHE[key] = func(*args)
        return MEMOIZATION_CACHE[key]
    return memoized_func

@memoize
def remake_can_string(can):
    """
    `remake_can_string` will use _obabel_ to recreate the smiles string it has
    been given. Because calling _obabel_ takes a lot of time, this function
    should be memoized.
    The obabel subprocess in this function is not resource limited.
    """
    pipe = subprocess.PIPE
    args = ['obabel', '-:%s' % can, '-o', 'can']
    proc = subprocess.Popen(args, stdin=pipe, stdout=pipe, stderr=pipe)
    (stdoutdata, stderrdata) = proc.communicate()
    return stdoutdata.strip()

def extract_refcode_from_sd(sd):
    return sd.splitlines()[0]
