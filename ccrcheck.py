#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import logging
import sys

import generate_can
import generate_can_ext
import verify_can
import smi2can

VERBOSITY_LEVELS = {
    0: logging.CRITICAL,
    1: logging.ERROR,
    2: logging.WARNING,
    3: logging.INFO,
    4: logging.DEBUG
}
DEFAULT_VERBOSITY = 2

def init_logging(args):
    verbosity_min = min(VERBOSITY_LEVELS.keys())
    verbosity_max = max(VERBOSITY_LEVELS.keys())
    verbosity = args.verbose - args.quiet
    if verbosity < verbosity_min:
        verbosity = verbosity_min
    elif verbosity > verbosity_max:
        verbosity = verbosity_max
    logging_level = VERBOSITY_LEVELS[verbosity]
    logging.basicConfig(level=logging_level)
    logging.info('logging level is %d' % verbosity)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='a routine for verifying if a cocrystal contains a list of coformers')

    group = parser.add_mutually_exclusive_group()
    group.add_argument('--verbose', '-v', action='count',
        default=DEFAULT_VERBOSITY, help='be (more) verbose')
    group.add_argument('--quiet', '-q', action='count',
        default=0, help='be (more) quiet')

    subparsers = parser.add_subparsers()
    generate_can_parser = subparsers.add_parser('generate-can',
        help='convert a mammut SD file to a CAN file')
    generate_can.build_parser(generate_can_parser)
    generate_can_ext_parser = subparsers.add_parser('generate-can-ext',
        help='convert a mammut SD file to a CAN file and immediately validate '
             'the result')
    generate_can_ext.build_parser(generate_can_ext_parser)
    smi2can_parser = subparsers.add_parser('smi2can',
    help='convert SMILES strings to CAN strings')
    smi2can.build_parser(smi2can_parser)
    verify_can_parser = subparsers.add_parser('verify-can',
        help='verify can strings using only the CAN string comparison method')
    verify_can.build_parser(verify_can_parser)

    args = parser.parse_args()

    if not hasattr(args, 'run'):
        parser.print_help()
        sys.exit(1)

    init_logging(args)
    args.run(args)
