#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import locale
import logging
import sys

import utils

if not locale.getlocale(locale.LC_NUMERIC):
    locale.setlocale(locale.LC_NUMERIC, '')

LOGGER = logging.getLogger('generate-can')

def build_parser(parser):
    parser.set_defaults(run=run)
    parser.add_argument('infile', type=argparse.FileType('r'), default=sys.stdin,
        help='the input file containing the SMILES strings structures')
    parser.add_argument('outfile', nargs="?", type=argparse.FileType('w'), default=sys.stdout,
        help='the file that will contain the produced CAN-strings (defaults to standard output)')


def run(args):
    infile = args.infile
    outfile = args.outfile

    try:
        generate_can(infile, outfile)
    finally:
        infile.close()
        outfile.close()

def generate_can(infile, outfile):
    for line in infile:
        smi, endl = line.rsplit('\t', 1)
        smi = bytes(smi, 'ascii')
        can = utils.remake_can_string(smi).decode('ascii')
        outfile.write("%s\t%s" % (can, endl))

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='convert a list of SMILES to canonical SMILES')
    build_parser(parser)
    args = parser.parse_args()
    run(args)
