#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import locale
import logging
import os
import subprocess
import resource
import sys

if not locale.getlocale(locale.LC_NUMERIC):
    locale.setlocale(locale.LC_NUMERIC, '')

LOGGER = logging.getLogger('generate-can')

def build_parser(parser):
    parser.set_defaults(run=run)
    parser.add_argument('sdfile', type=argparse.FileType('r'), default=sys.stdin,
        help='the input file containing the SD structures')
    parser.add_argument('outfile', nargs="?", type=argparse.FileType('w'), default=sys.stdout,
        help='the input file that will contain the produced CAN-strings (defaults to standard output)')
    time_default = 600
    parser.add_argument('-t', '--timeout', type=int, default=time_default, metavar='time',
        help='the maximum timeout for Open Babel per structure in seconds (default: %s)' %
             locale.format('%d', time_default, grouping=True))
    memory_default = 1000000
    parser.add_argument('-m', '--max-memory', type=int, default=memory_default, metavar='mem',
        help='the memory cap for Open Babel in kilobytes (default: %s)' %
             locale.format('%d', memory_default, grouping=True))


def get_default_outfile(infile_filename):
    basename = os.path.splitext(infile_filename)[0]
    return open('%s.can' % basename, 'w')

def run(args):
    infile = args.sdfile
    outfile = args.outfile
    timeout = args.timeout
    max_memory = args.max_memory

    try:
        generate_cans(infile, outfile, timeout, max_memory)
    finally:
        infile.close()
        outfile.close()

def generate_cans(infile, outfile, timeout, max_memory):
    for sd in sd_generator(infile):
        can_line = generate_can(sd, timeout, max_memory)
        if can_line:
            outfile.write(can_line)

def sd_generator(f):
    sd = ""
    for line in f:
        sd += line
        if line.strip() == '$$$$':
            yield sd
            sd = ""

def generate_can(sd, timeout, max_memory):
    refcode = sd.split('\n', 1)[0].strip()

    args = ['babel', '-i', 'sd', '-', '-o', 'can', '-']
    p = subprocess.Popen(args, universal_newlines=True, stdin=subprocess.PIPE,
                         stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    # limit the process' memory to 1GB (default)
    limit = (1024 * max_memory, resource.RLIM_INFINITY)
    resource.prlimit(p.pid, resource.RLIMIT_AS, limit)

    try:
        out, err = p.communicate(sd, timeout=timeout)
        if p.returncode == 127:
            raise MemoryError(err)
        elif p.returncode:
            LOGGER.warning('unexpected return code %d after running Open '
                    'Babel on %s, skipping entry' % (p.returncode, refcode))
            return None
    except MemoryError:
        LOGGER.warning('Open Babel ran out of memory converting %s, '
                        'skipping entry' % refcode)
        return None
    except subprocess.TimeoutExpired:
        LOGGER.warning('timeout while running Open Babel on %s, '
                        'skipping entry' % refcode)
        return None

    return out


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='convert a SD mammut file to a CAN file')
    build_parser(parser)
    args = parser.parse_args()
    run(args)
