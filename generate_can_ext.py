#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import locale
import logging
import os
import subprocess
import resource
import sys

import generate_can
import utils
from verify_can import build_smiles_cache, naive_can_match, simplify_smi, get_containing_smiles, filter_loose_atoms

if not locale.getlocale(locale.LC_NUMERIC):
    locale.setlocale(locale.LC_NUMERIC, '')

CSD_SMILES_CACHE = {}
LOGGER = logging.getLogger('generate-can')

def build_parser(parser):
    generate_can.build_parser(parser)
    parser.set_defaults(run=run)
    parser.add_argument('--reference-smi', type=argparse.FileType('r'),
        required=True,
        help='the reference SMILES strings, as directly produced by the CSD '
             'to be used to check the validity of the produced SMILES strings')

def generate_cans(infile, outfile, timeout, max_memory):
    for sd in generate_can.sd_generator(infile):
        # 1) Traditional can generation step
        refcode = utils.extract_refcode_from_sd(sd)
        can_ref = CSD_SMILES_CACHE[refcode]
        can_line = generate_can.generate_can(sd, timeout, max_memory)

        # 2) Added verification
        out = None
        if not can_line:
            # Panick happened
            out = '<panick>\t{}\n'.format(refcode)
            outfile.write(out)
            continue

        can, other_refcode = [x.strip() for x in can_line.split('\t', 1)]
        can = bytes(can, 'ascii')
        assert refcode == other_refcode, 'generated refcode did not match SD refcode'

        # extract the coformer SMILES strings from the can strings
        cans = list(get_containing_smiles(can))
        cans_ref = list(get_containing_smiles(can_ref))

        if not any(can for can in cans):
            # There were no coordinates in the SD structure
            out = '<nocoords>\t{}\n'.format(refcode)
            outfile.write(out)
            continue

        # remove singleton atoms
        cans = filter_loose_atoms(cans)

        if not any(naive_can_match(can, ref) for ref in cans_ref
                                             for can in cans):
            out = '<invcan>\t{}\n'.format(refcode)
            outfile.write(out)
            continue

        out = can_line
        outfile.write(out)

def run(args):
    infile = args.sdfile
    outfile = args.outfile
    csd_canfile = args.reference_smi
    timeout = args.timeout
    max_memory = args.max_memory

    # build the reference CSD smiles cache
    LOGGER.info("loading reference SMILES")
    build_smiles_cache(csd_canfile, CSD_SMILES_CACHE, simplify_smi)
    csd_canfile.close()

    try:
        generate_cans(infile, outfile, timeout, max_memory)
    finally:
        infile.close()
        outfile.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='convert a SD mammut file to a CAN file')
    build_parser(parser)
    args = parser.parse_args()
    run(args)
