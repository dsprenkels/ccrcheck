#!/usr/bin/env python
# -*- coding: utf-8 -*-

import operator
import argparse
import sys
import itertools
import logging
import subprocess
import json

import utils

CSD_SMILES_CACHE = {}
BABEL_SMILES_CACHE = {}
LOGGER = logging.getLogger('verify-can')

class ValidationException(Exception):
    pass

class NotOneStructure(ValidationException):
    pass

def build_parser(parser):
    parser.set_defaults(run=run)
    parser.add_argument('--out', type=argparse.FileType('w'),
        default=sys.stdout,
        help='the output TSV file, containing the verification results '
             '(defaults to standard output)')
    parser.add_argument('--can', type=argparse.FileType('r'),
        required=True,
        help='the input file containing the CAN strings, as procuced using '
             'the `generate-can` command')
    parser.add_argument('--lst', type=argparse.FileType('r'),
        required=True,
        help='the input LST files, containing the cocrystal records that '
             'need to be checked')
    parser.add_argument('--reference-smi', type=argparse.FileType('r'),
        required=True,
        help='the reference SMILES strings, as directly produced by the CSD '
             'to be used to check the validity of the produced SMILES strings')
    parser.add_argument('--no-show-skips', action='store_true', default=False,
        help="Filter skipped entries from the results (default: show)")
    parser.add_argument('--group-results', action='store_true', default=False,
        help='Group records, if one match is found for a tuple of the same '
             '"base", don\'t go examining the rest (default: False)')


def run(args):
    try:
        outfile = args.out
        canfile = args.can
        lstfile = args.lst
        csd_canfile = args.reference_smi
        options = {
            'show_skips': not args.no_show_skips,
            'group_results': args.group_results
        }
        verify_can_strings(outfile, canfile, lstfile, csd_canfile, **options)
    except KeyboardInterrupt:
        LOGGER.info("KeyboardIntterupt received, aborting")

def verify_can_strings(outfile, canfile, lstfile, csd_canfile, **options):
    # build the produced smiles cache
    LOGGER.info("loading CAN strings")
    build_smiles_cache(canfile, BABEL_SMILES_CACHE)
    canfile.close()

    # build the reference CSD smiles cache
    LOGGER.info("loading reference SMILES")
    build_smiles_cache(csd_canfile, CSD_SMILES_CACHE, simplify_smi)
    csd_canfile.close()

    # parsing the input lst file
    LOGGER.info("loading lst file")
    lst = get_lstfile_records(lstfile)
    lstfile.close()

    LOGGER.info("verifying smiles strings")
    for key, group in lst:
        results = process_record_group(key, group, **options)
        for result in results:
            print(*result[0], sep='\t', end='\t', file=outfile)
            print(json.dumps(result[1]), file=outfile)

def get_lstfile_records(lstfile):
    """
    This function reads fileobj `lstfile`'s and parses its contents
    """
    def keyfunc(x):
        return tuple(refcode[:6] for refcode in x[:3])
    lstfile_lines = lstfile.readlines()
    lstfile_records = (tuple(line.strip().split())
                       for line in lstfile_lines)
    lstfile_records = (tuple([line[0]] + sorted(line[1:3]))
                       for line in lstfile_records)
    return itertools.groupby(sorted(lstfile_records, key=keyfunc), key=keyfunc)

def process_record_group(key, group, **options):
    """
    `process_record_group` is a wrapper around process_record which processes
    the groups of records with the same real structures (only different
    refcodes and thus different CSD structures)
    `key` should be a tuple containing the "base structures". While at the
    moment it is unused, in the future this will probablybe used for
    specifying the group in output data.
    """
    results = []
    for record in group:
        result = (record, process_record(record))
        LOGGER.debug(result)
        if options['group_results'] and result[1][0] == 'result/match':
            return [result]
        if not options['show_skips'] and result[1][0].startswith('skip/'):
            continue
        results.append(result)
    if options['group_results']:
        try:
            first_real_result = next(result for result in results
                                     if result[1][0].startswith('result'))
            return [first_real_result]
        except StopIteration:
            if results:
                return [results[0]]
            else:
                # `results` is empty because there were only skipped entries
                return []
    else:
        return results

    raise AssertionError('this state should be unreachable')

def process_record(refcodes):
    """
    `process_record` processes one LST-formatted refcodes list; for example:
        ['ABEKUN01', 'AZSTBB', 'EVUSIX01']
    it will return a tuple of ``(status, params, message)``
    `status` may be one of:
        - 'result/<type>'
        - 'skip/<type>'
    <type> will by a computer-readable "explanation" to status
        - if status is 'skip', `params` will provide one of the following:
            * when type is 'panick', x =>
                Open Babel was killed (SIGKILL) during during the conversion
                of `x` (list). This probably means the input SD file was
                unreadable.
            * when type is 'nocoords', x =>
                to the structures provided in `x` no coordinates were provided.
            * when type is 'notonestring', x) =>
                of the structures provided in `x`, in the case of the coformers
                it was not possible to extract only one CAN string. This is
                mostly the case when one of the coformer
            * when type is 'invcan', x =>
                The produced CAN string was matched against the reference
                SMILES string from the CSD. It is assumed that Babel produced
                an invalid CAN string.
        - if status is 'result', `params` will be:
            * ('match',) =>
                A match was found.
            * ('mismatch', x) =>
                A mismatch occured. `x` is a list of coformer refcodes that
                had a mismatch.
    `message` will contain a more human-readable explanation with the status
    code and should not be used for parsing.
    """
    # did a structure fail to convert?
    panicked = []
    for refcode in refcodes:
        if not refcode in BABEL_SMILES_CACHE:
            panicked.append(refcode)
    if panicked:
        msg = 'conversion failed with %s' % panicked
        return ('skip/panick', panicked, msg)

    check = []
    for refcode in refcodes:
        # extract the multiple smiles string (seperated by '.') from each
        cans = list(get_containing_smiles(BABEL_SMILES_CACHE[refcode]))
        cans_ref = list(get_containing_smiles(CSD_SMILES_CACHE[refcode]))
        check.append([refcode, cans, cans_ref])

    # do we even have coordinates available for each structure?
    nocoords = []
    for i, (refcode, cans, refs) in enumerate(check):
        if not any(can for can in cans):
            nocoords.append(refcode)
    if nocoords:
        msg = 'no coordinates available for %s' % nocoords
        return ('skip/nocoords', nocoords, msg)

    # filter each string, so that singleton atoms are removed
    for i, (refcode, cans, refs) in enumerate(check):
        check[i][1] = filter_loose_atoms(cans)

    # for each ligand, there should be exactly one string left
    notone_refcodes = []
    notone_cans = []
    for lig in check[1:]:
        if not len(lig[1]) == 1:
            notone_refcodes.append(lig[0])
            notone_cans.append([can.decode() for can in lig[1]])
            continue
        else:
            lig[1] = lig[1][0]
    assert(len(notone_refcodes) == len(notone_cans))
    if notone_refcodes:
        msg = '%s did not produce one CAN string' % notone_refcodes
        return ('skip/notonestring', (notone_refcodes, notone_cans), msg)

    # sanity check: can strings should match reference can string naively
    sanity_failed = []
    refcode, cans, refs = check[0]
    if not any(naive_can_match(can, ref) for ref in refs
                                         for can in cans):
        sanity_failed.append((refcode, cans, refs))
    for refcode, can, refs in check[1:]:
        if not any(naive_can_match(can, ref) or
                   naive_can_match(can, utils.remake_can_string(ref))
                   for ref in refs):
            sanity_failed.append((refcode, can, refs))
    if sanity_failed:
        msg = 'reference check failed for %s' % sanity_failed
        refcodes = next(zip(*sanity_failed))
        return ('skip/invcan', refcodes, msg)

    # at this point, of the record with the structures in `crrcheck[0][1]`
    # (n structres) and `ccrcheck[{1,2}][1]`` (each one structure) we should
    # be able to tell if it is a match or not, so we can yield "{,no}match".
    mismatch = []
    ccrcans = check[0][1]
    for i, (refcode, can, refs) in enumerate(check[1:]):
        if not any(can_match(can, ccrcan) for ccrcan in ccrcans):
            mismatch.append(refcode)

    if mismatch:
        msg = '%s did not occur in %s' % (mismatch, refcodes[0])
        return ('result/mismatch', mismatch, msg)
    else:
        msg = '%s all occured in %s' % (refcodes[1:], refcodes[0])
        return ('result/match', None, msg)

    raise AssertionError('this state should be unreachable')


def get_containing_smiles(lig_can, sep=bytes('.', 'ascii')):
    """
    `get_containing_smiles` is a generator function that yields all the
    coformer structures in `lig_can`. Because of that a different CAN string
    may have been produced by OpenBabel, this function will convert the strings
    again, apart from each other if multiple different CAN strings appear in
    `lig_can`. Note: If the input is None, this generator will yield None once.
    """
    if not sep in lig_can:
        yield lig_can
    else:
        lig_can_split = sorted(list(set(lig_can.split(sep))))
        for can in lig_can_split:
            yield utils.remake_can_string(can)

def extract_smiles_string(line):
    if line == '':
        raise IOError('encoutered empty string which should contain a '
                      'smiles string (and a refcode)')
    smiles_string, refcode = line.rsplit("\t", 1).strip()
    if smiles_string == '':
        raise ValidationException('%s has an empty smiles string' % refcode.strip())
    return smiles_string

def naive_can_match(*cans):
    cans = [can.translate(None, b'[]H@/\\') for can in cans]
    return len(set(cans)) == 1

def can_match(*cans):
    cans = [can.translate(None, b'[]H') for can in cans]
    return len(set(cans)) == 1

def simplify_smi(smi):
    return smi.replace('[', '').replace(']', '')

def build_smiles_cache(canfile, cache, process=lambda x: x):
    for line in canfile:
        if not line.strip(): continue
        can, refcode = line.rstrip().rsplit('\t', 1)
        can = process(can)
        cache[refcode] = bytes(can, 'ascii')

def filter_loose_atoms(orig_cans):
    cans = [can for can in orig_cans
            if not simplify_smi(can.decode('ascii')) in utils.ATOMS]
    return cans if cans else orig_cans
